package net.daemonyum.banshee.framework.web.service;

import net.daemonyum.banshee.framework.web.bean.FormBean;
import net.daemonyum.banshee.framework.web.configurator.ConfiguratorTools;
import net.daemonyum.banshee.framework.web.configurator.RedirectConfigurator;
import net.daemonyum.banshee.framework.web.utils.Constant;
import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.exception.ForbiddenException;
import net.daemonyum.manticora.framework.web.exception.WebException;
import net.daemonyum.manticora.framework.web.service.servlet.AjaxServlet;
import net.daemonyum.manticora.framework.web.service.view.ServletView;

import org.apache.log4j.Logger;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class DescriptionServlet extends AjaxServlet {
	
	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(DescriptionServlet.class);
	
	RedirectConfigurator redirectServlet;

	@Override
	public ManticoraBean preExecute() {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] preExecute");
		try {
			redirectServlet = ConfiguratorTools.getInstance().redirectConfiguratorFromServlet(this.getClass());
		} catch(IllegalArgumentException e) {
			LOGGER.trace("["+this.getClass().getSimpleName()+"] "+ "pas de bean de validation");
		}
		
		if(hasRightToViewThisPage()) {
			
			FormBean bean = null;
			if(redirectServlet != null && redirectServlet.getForm() != null) {
				try {
					bean = redirectServlet.getForm().newInstance();
					bean.fill(this);
				} catch (InstantiationException e) {
					bean = null;
				} catch (IllegalAccessException e) {
					bean = null;
				}
			}else {
				bean = fillBeanWithParameters();
			}
			if(bean == null) {
				throw new WebException(getClass().getSimpleName() + " FormBean is null.");
			}
			bean.doValidation();			
			return bean;
		}else {
			throw new ForbiddenException(Constant.SECURITY_EXCEPTION_MESSAGE);
		}
	}
		
	@Override
	public ServletView doAjax(ManticoraBean bean) {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doAjax");
		// non accessible à un bean null mais on teste qd même		
		FormBean rainbowBean = (FormBean) bean;
		
		if(rainbowBean != null && rainbowBean.isValid()) {				
			doService();
		}			
		
		ServletView view = new ServletView(ConfiguratorTools.getInstance().jspConfiguratorFromServlet(this.getClass(),null));			
		return view;		
	}
	
	public abstract FormBean fillBeanWithParameters() throws WebException;
	
	public abstract void doService();
	
	public abstract boolean hasRightToViewThisPage();
	
	@Override
	public void destroy(){}

}
