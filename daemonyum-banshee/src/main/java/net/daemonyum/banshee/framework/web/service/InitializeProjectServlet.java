package net.daemonyum.banshee.framework.web.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import net.daemonyum.banshee.framework.web.configurator.Configurator;
import net.daemonyum.banshee.framework.web.configurator.ConfiguratorTools;
import net.daemonyum.banshee.framework.web.configurator.ServletConfigurator;
import net.daemonyum.banshee.framework.web.configurator.StaticConfigurator;
import net.daemonyum.banshee.framework.web.utils.Config;
import net.daemonyum.banshee.framework.web.utils.Constant;
import net.daemonyum.manticora.framework.web.service.servlet.LoadOnStartupServlet;
import net.daemonyum.manticora.framework.web.service.servlet.manager.ContextAttribute;
import net.daemonyum.manticora.framework.web.service.servlet.manager.ContextAttributeType;

@SuppressWarnings("serial")
public abstract class InitializeProjectServlet extends LoadOnStartupServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(InitializeProjectServlet.class);

	private Configurator configurator;
	private ServletContext context;
	private String extensionUrl;

	public InitializeProjectServlet(Configurator configurator, String extensionUrl) {
		this.configurator = configurator;
		this.extensionUrl = extensionUrl;
	}

	@Override
	protected void doInit(ServletContext context) {
		this.context = context;
		initializeContext();
		initializeConfiguration();
	}

	protected abstract void initializeContext();


	private void initializeConfiguration() {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] initializeConfiguration");

		// appeler l'init Config de banshee
		Config.init(Constant.BASENAME);

		ServletConfigurator[] servletConfigurators = configurator.getServletConf();
		//ConfiguratorTools.addStaticServlet(configurator.getServletConf(),extensionUrl);

		for(ServletConfigurator servletConfig : servletConfigurators) {

			if(servletConfig.getServlet() != null) {
				String prefix = context.getServletContextName().isEmpty()?"":"/"+context.getServletContextName();
				String key = prefix+"/"+servletConfig.getUrlWithoutExtension()+"."+extensionUrl;
				LOGGER.info(key+" --->"+servletConfig.getServlet().getCanonicalName() );

				Map<ContextAttributeType, ContextAttribute> contextMap = new HashMap<ContextAttributeType, ContextAttribute>();
				contextMap.put(ContextAttributeType.SERVLET, ContextAttribute.builder().servlet(servletConfig.getServlet()).build());
				context.setAttribute(key, contextMap);			
				context.setAttribute(servletConfig.getApplicationLink(), servletConfig.getUrlWithoutExtension()+"."+extensionUrl);
			}
		}

		if(configurator.getStaticConf() != null) {
			for(StaticConfigurator staticConfig : configurator.getStaticConf()) {
				context.setAttribute(staticConfig.name(), staticConfig.getPath());
			}
		}

		ConfiguratorTools.init(servletConfigurators,configurator.getJspConf(),configurator.getRedirectConf(),configurator.getMobileConf());

		LOGGER.info("***********************************************");
		LOGGER.info("***********************************************");
		LOGGER.info("*** This is a Daemon-Rainbow application V1 ***");
		LOGGER.info("***********************************************");
		LOGGER.info("***********************************************");
	}

}
