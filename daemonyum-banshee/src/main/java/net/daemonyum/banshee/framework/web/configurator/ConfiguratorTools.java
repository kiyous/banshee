package net.daemonyum.banshee.framework.web.configurator;

import org.apache.log4j.Logger;

import net.daemonyum.banshee.framework.web.utils.Device;
import net.daemonyum.manticora.framework.web.service.servlet.ManticoraServlet;

/**
 * 
 * @author Kiyous
 *
 */
public final class ConfiguratorTools {

	private static ConfiguratorTools instance;

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(ConfiguratorTools.class);

	private JSPConfigurator[] jspConfiguratorList;
	private JSPConfigurator[] mobileConfiguratorList;
	private RedirectConfigurator[] redirectConfiguratorList;
	private ServletConfigurator[] servletConfiguratorList;

	private ConfiguratorTools(ServletConfigurator[] servletConfiguratorList,JSPConfigurator[] jspConfiguratorList,RedirectConfigurator[] redirectConfiguratorList,JSPConfigurator[] mobileConfiguratorList) {
		this.servletConfiguratorList = servletConfiguratorList;
		this.jspConfiguratorList = jspConfiguratorList;
		this.redirectConfiguratorList = redirectConfiguratorList;
		this.mobileConfiguratorList = mobileConfiguratorList;
	}

	public JSPConfigurator jspConfiguratorFromName(String name, Device device) {
		for(JSPConfigurator jspConf: jspConfiguratorList) {

			if(device != null) {
				switch(device) {
					case MOBILE:
						if(jspConf.name().equals(name) && jspConf.hasMobileVersion()) {
							return mobileConfiguratorFromName(name);
						}else if(jspConf.name().equals(name) && !jspConf.hasMobileVersion()){
							return jspConf;
						}
						break;

					default:
						if(jspConf.name().equals(name)) {
							return jspConf;
						}
						break;
				}
			}else {
				if(jspConf.name().equals(name)) {
					return jspConf;
				}				
			}


		}
		throw new IllegalArgumentException("Le nom "+name+" n'est pas répertorié dans les paramètres de configuration");
	}

	public JSPConfigurator jspConfiguratorFromServlet(Class<? extends ManticoraServlet> servlet, Device device) {
		for(ServletConfigurator servletConf: servletConfiguratorList) {
			if(servletConf.getServlet()!= null && servletConf.getServlet().equals(servlet)) {
				return jspConfiguratorFromName(servletConf.name(), device);
			}
		}
		throw new IllegalArgumentException("La servlet "+servlet.getCanonicalName()+" n'est pas répertorié dans les paramètres de configuration");
	}	

	public JSPConfigurator mobileConfiguratorFromName(String name) {
		for(JSPConfigurator jspConf: mobileConfiguratorList) {			
			if(jspConf.name().equals(name)) {
				return jspConf;
			}						
		}
		throw new IllegalArgumentException("Le nom "+name+" n'est pas répertorié dans les paramètres de configuration");
	}

	public RedirectConfigurator redirectConfiguratorFromName(String name) {
		for(RedirectConfigurator redirectConf: redirectConfiguratorList) {
			if(redirectConf.name().equals(name)) {
				return redirectConf;
			}
		}
		throw new IllegalArgumentException("Le nom "+name+" n'est pas répertorié dans les paramètres de configuration");
	}

	public RedirectConfigurator redirectConfiguratorFromServlet(Class<? extends ManticoraServlet> servlet) {
		for(ServletConfigurator servletConf: servletConfiguratorList) {
			if(servletConf.getServlet()!= null && servletConf.getServlet().equals(servlet)) {
				return redirectConfiguratorFromName(servletConf.name());
			}
		}
		throw new IllegalArgumentException("La servlet "+servlet.getCanonicalName()+" n'est pas répertorié dans les paramètres de configuration");
	}

	public ServletConfigurator servletConfiguratorFromServlet(Class<? extends ManticoraServlet> servlet) {
		for(ServletConfigurator servletConf: servletConfiguratorList) {
			if(servletConf.getServlet().equals(servlet)) {
				return servletConf;
			}
		}
		throw new IllegalArgumentException("La servlet "+servlet.getCanonicalName()+" n'est pas r�pertori� dans les param�tres de configuration");
	}

	public static ConfiguratorTools getInstance() {
		return instance;
	}

	/**
	 * Initialise les variables d'application.
	 * @param servletConfiguratorList
	 * @param jspConfiguratorList
	 * @param redirectConfiguratorList
	 * @param mobileConfiguratorList
	 */
	public static void init(ServletConfigurator[] servletConfiguratorList,JSPConfigurator[] jspConfiguratorList,RedirectConfigurator[] redirectConfiguratorList,JSPConfigurator[] mobileConfiguratorList) {
		if(instance == null) {
			instance = new ConfiguratorTools(servletConfiguratorList, jspConfiguratorList, redirectConfiguratorList,mobileConfiguratorList);
		}else {
			LOGGER.info("Les paramètres d'application sont déjà instanciés.");
		}
	}

	//	public static ServletConfigurator[] addStaticServlet(ServletConfigurator[] defaultServletConf, final String extension) {
	//		
	//		ServletConfigurator staticServletConfigurator = new ServletConfigurator() {
	//			
	//			public String name() {
	//				return "STATIC";
	//			}
	//			
	//			public boolean hasJsp() {
	//				return false;
	//			}
	//			
	//			public String getUrlWithoutExtension() {
	//				return "static";
	//			}
	//			
	//			public Class<? extends DaemonyumServlet> getServlet() {
	//				return DaemonyumStreamServlet.class;
	//			}
	//			
	//			public String getApplicationLink() {
	//				return "static";
	//			}
	//		};
	//		
	//		ServletConfigurator[]servletConfiguratorArray = new ServletConfigurator[defaultServletConf.length+1];
	//		
	//		for(int i=0;i<defaultServletConf.length;i++) {
	//			servletConfiguratorArray[i] = defaultServletConf[i];
	//		}
	//		servletConfiguratorArray[servletConfiguratorArray.length-1] = staticServletConfigurator;		
	//		
	//		return servletConfiguratorArray;
	//	}

}
