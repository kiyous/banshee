package net.daemonyum.banshee.framework.web.service;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import net.daemonyum.ajatar.annotation.data.FillFromSession;
import net.daemonyum.ajatar.annotation.data.SendToRequest;
import net.daemonyum.ajatar.parameter.Clean;
import net.daemonyum.banshee.framework.web.configurator.ConfiguratorTools;
import net.daemonyum.banshee.framework.web.service.response.View;
import net.daemonyum.banshee.framework.web.utils.Constant;
import net.daemonyum.banshee.framework.web.utils.Device;
import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.exception.ForbiddenException;
import net.daemonyum.manticora.framework.web.service.servlet.GetServlet;
import net.daemonyum.manticora.framework.web.service.view.ServletView;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class JspServlet extends GetServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(JspServlet.class);

	@FillFromSession(value=Constant.BANSHEE_SESSION_CONFIRM_MESSAGE_PARAMETER,clean=Clean.TRUE)
	@SendToRequest("confirmMessage")
	protected String confirm;

	@FillFromSession(value=Constant.BANSHEE_SESSION_ERRORS_MESSAGE_PARAMETER,clean=Clean.TRUE)
	@SendToRequest("errors")
	protected List<String> errors;

	private View rainbowView;

	@SendToRequest("sticker")
	private String servletLabel;

	private String userAgent;

	@Override
	public void destroy() {

	}

	@Override
	public ServletView doGet() {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doGet");
		Device device = Device.detect(userAgent);
		ServletView view = new ServletView(ConfiguratorTools.getInstance().jspConfiguratorFromServlet(this.getClass(),device));

		doService();
		if(!rainbowView.equals(View.MAP)) {
			view.changeContent(rainbowView.getView());
		}		
		return view;
	}	

	public abstract void doService();

	public abstract boolean hasRightToViewThisPage();

	@Override
	public ManticoraBean preExecute() {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] preExecute");
		if(hasRightToViewThisPage()) {
			if(errors == null) {
				errors = new LinkedList<String>();
			}
			servletLabel = ConfiguratorTools.getInstance().servletConfiguratorFromServlet(this.getClass()).getUrlWithoutExtension();
			rainbowView = View.MAP;
			userAgent = getRequest().getHeader("User-Agent");
			LOGGER.trace("["+this.getClass().getSimpleName()+"] user-agent: "+userAgent);
			return null;
		}else {
			LOGGER.error("Tentative d'accès à " + getClass().getName());
			throw new ForbiddenException(Constant.SECURITY_EXCEPTION_MESSAGE);
		}
	}	

	public void showAlternativeView(View rainbowView) {
		this.rainbowView = rainbowView;
	}

}
