package net.daemonyum.banshee.framework.web.service;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import net.daemonyum.ajatar.annotation.data.SendToSession;
import net.daemonyum.banshee.framework.web.bean.FormBean;
import net.daemonyum.banshee.framework.web.configurator.ConfiguratorTools;
import net.daemonyum.banshee.framework.web.configurator.RedirectConfigurator;
import net.daemonyum.banshee.framework.web.configurator.ServletConfigurator;
import net.daemonyum.banshee.framework.web.utils.Constant;
import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.exception.ForbiddenException;
import net.daemonyum.manticora.framework.web.exception.WebException;
import net.daemonyum.manticora.framework.web.service.servlet.ServiceServlet;
import net.daemonyum.manticora.framework.web.service.view.ServletRedirectUrl;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class ActionServlet extends ServiceServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(ActionServlet.class);

	@SendToSession(Constant.BANSHEE_SESSION_CONFIRM_MESSAGE_PARAMETER)
	protected String confirm;

	@SendToSession(Constant.BANSHEE_SESSION_ERRORS_MESSAGE_PARAMETER)
	protected List<String> errors;

	private boolean isSuccess = false;

	RedirectConfigurator redirectServlet;

	@Override
	public void destroy() {}

	public abstract void doAction(FormBean bean);	

	@Override
	public ServletRedirectUrl doGetOrPost(ManticoraBean bean) {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doGetOrPost");
		// non accessible à un bean null mais on teste qd même		
		FormBean rainbowBean = (FormBean) bean;
		if(rainbowBean == null) {
			setSuccessAction(false);
			errors.add("FATAL ERROR: impossible d'effectuer la validation.");
		}else {
			if(rainbowBean.isValid()) {		
				setSuccessAction(true);
				doAction(rainbowBean);
			} else {
				setSuccessAction(false);
				errors.addAll(rainbowBean.getErrors());	
				fail(rainbowBean);
			}
		}

		ServletConfigurator servlet;
		if(isSuccess) {
			servlet = redirectServlet.getSuccessServlet();
		}else {
			servlet = redirectServlet.getErrorServlet();
		}
		return new ServletRedirectUrl((String)getContext().getAttribute(servlet.getApplicationLink()));		
	}

	public abstract void fail(FormBean bean);

	public abstract FormBean fillBeanWithParameters() throws WebException;

	public abstract boolean hasRightToViewThisPage();

	@Override
	public ManticoraBean preExecute() {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] preExecute");
		try {
			redirectServlet = ConfiguratorTools.getInstance().redirectConfiguratorFromServlet(this.getClass());
		} catch(IllegalArgumentException e) {
			LOGGER.warn("["+this.getClass().getSimpleName()+"] ",e);
		}

		if(hasRightToViewThisPage()) {
			errors = new LinkedList<String>();
			FormBean bean = null;
			if(redirectServlet != null && redirectServlet.getForm() != null) {
				try {
					bean = redirectServlet.getForm().newInstance();
					bean.fill(this);
				} catch (InstantiationException e) {
					bean = null;
				} catch (IllegalAccessException e) {
					bean = null;
				}
			}else {
				bean = fillBeanWithParameters();
			}
			if(bean == null) {
				throw new WebException(getClass().getSimpleName() + " FormBean is null.");
			}
			bean.doValidation();			
			return bean;
		}else {
			throw new ForbiddenException(Constant.SECURITY_EXCEPTION_MESSAGE);
		}
	}

	protected void addErrorMessage(String message) {
		this.errors.add(message);
	}

	protected void setConfirmMessage(String message) {
		this.confirm = message;
	}

	protected void setSuccessAction(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

}
