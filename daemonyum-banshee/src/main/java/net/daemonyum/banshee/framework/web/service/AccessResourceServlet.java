package net.daemonyum.banshee.framework.web.service;

import org.apache.log4j.Logger;

import net.daemonyum.banshee.framework.web.bean.ServiceEntity;
import net.daemonyum.banshee.framework.web.utils.Device;
import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.bean.ManticoraEntity;
import net.daemonyum.manticora.framework.web.service.servlet.RestServlet;
import net.daemonyum.manticora.framework.web.service.view.ServletResource;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class AccessResourceServlet extends RestServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(AccessResourceServlet.class);


	private String userAgent;

	@Override
	public void destroy() {

	}

	public abstract ServletResource doCreateTreatment(ServiceEntity webEntity);

	@Override
	public ServletResource doDelete(ManticoraEntity entity) {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doDelete");
		Device device = Device.detect(userAgent);
		ServletResource resource = doDeleteTreatment((ServiceEntity)entity);
		return resource;
	}

	public abstract ServletResource doDeleteTreatment(ServiceEntity webEntity);

	@Override
	public ServletResource doGet() {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doGet");
		Device device = Device.detect(userAgent);
		ServletResource resource = doSelectTreatment();
		return resource;
	}

	@Override
	public ServletResource doPost(ManticoraEntity entity) {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doPost");
		Device device = Device.detect(userAgent);
		ServletResource resource = doUpdateTreatment((ServiceEntity)entity);
		return resource;
	}

	@Override
	public ServletResource doPut(ManticoraEntity entity) {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doPut");
		Device device = Device.detect(userAgent);
		ServletResource resource = doCreateTreatment((ServiceEntity)entity);
		return resource;
	}
	public abstract ServletResource doSelectTreatment();
	public abstract ServletResource doUpdateTreatment(ServiceEntity webEntity);
	//	public abstract FormBean fillBeanWithParameters() throws WebException;

	@Override
	public ManticoraBean preExecute() {
		return null;
		//		FormBean bean = null;
		//
		//		bean = fillBeanWithParameters();
		//
		//		if(bean == null) {
		//			throw new WebException(getClass().getSimpleName() + " FormBean is null.");
		//		}
		//		bean.doValidation();			
		//		return bean;
	}

}
