package net.daemonyum.banshee.framework.web.utils;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import net.daemonyum.manticora.framework.web.exception.WebException;


/**
 * Classe de configuration de l'application
 * Contient toutes les données du fichier de configuration
 *
 */
public class Config {

	// Liste des variables tampons
	public static String SECURITY_ERROR_MESSAGE    = null;

	private static boolean isInitialized = false;

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(Config.class);

	private static final String MISSING_PROPERTY = "Missing banshee configuration property: ";

	// Liste des clés contenus dans le fichier de config
	private static final String SECURITY_ERROR_MESSAGE_KEY     = "banshee.error.security.exception.message";

	public static void init(String baseName) {

		ResourceBundle bundle = null;		

		if(!isInitialized) {

			bundle = ResourceBundle.getBundle(baseName);			


			if(bundle.containsKey(SECURITY_ERROR_MESSAGE_KEY)) {
				SECURITY_ERROR_MESSAGE = bundle.getString(SECURITY_ERROR_MESSAGE_KEY);
				if(SECURITY_ERROR_MESSAGE != null) {
					SECURITY_ERROR_MESSAGE = SECURITY_ERROR_MESSAGE.trim();
				}
			}else {
				throw new WebException(MISSING_PROPERTY + SECURITY_ERROR_MESSAGE_KEY);
			}



			LOGGER.info("***************** CONTENU DE LA CONF **********************");
			LOGGER.info(SECURITY_ERROR_MESSAGE_KEY  + " = " + SECURITY_ERROR_MESSAGE);
			LOGGER.info("*************************************************************");

			isInitialized = true;

		}
	}
}
