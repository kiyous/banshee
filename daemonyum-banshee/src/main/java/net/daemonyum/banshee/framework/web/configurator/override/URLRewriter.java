package net.daemonyum.banshee.framework.web.configurator.override;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import net.daemonyum.banshee.framework.web.configurator.ServletConfigurator;
import net.daemonyum.manticora.framework.web.service.servlet.manager.ContextAttribute;
import net.daemonyum.manticora.framework.web.service.servlet.manager.ContextAttributeType;

public class URLRewriter {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(URLRewriter.class);

	private ServletContext context;

	public URLRewriter(ServletContext context) {
		this.context = context;
	}

	public void add(CombinedView combinedView) {
		String transformToUrl = combinedView.getSpecificURL();
		ServletConfigurator servletConfig = combinedView.getPackagedURL().getServletConfig();
		Map<String,String> parameters = combinedView.getPackagedURL().getParameters();
		String key = context.getServletContextName()+"/"+ transformToUrl ;
		Map<ContextAttributeType, ContextAttribute> contextMap = new HashMap<ContextAttributeType, ContextAttribute>();
		contextMap.put(ContextAttributeType.SERVLET, ContextAttribute.builder().servlet(servletConfig.getServlet()).build());
		contextMap.put(ContextAttributeType.PARAMS, ContextAttribute.builder().parameters(parameters).build());
		context.setAttribute(key, contextMap);

		LOGGER.trace("urlrewriting: " + key + " --> " + servletConfig.getServlet().getCanonicalName());
		for(String mapKey:parameters.keySet()) {
			LOGGER.trace("urlrewriting: " + mapKey +"="+ parameters.get(mapKey));
		}
	}

	public static String normalize(String textToTransform) {
		textToTransform = Normalizer.normalize(textToTransform, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");// sans accent		
		textToTransform = textToTransform.replaceAll("\\W", "-");// sans espace ni ponctuation
		textToTransform = textToTransform.toLowerCase();// en minuscule
		textToTransform = textToTransform.replaceAll("---", "-");// virer les triplets
		return textToTransform;
	}

}
