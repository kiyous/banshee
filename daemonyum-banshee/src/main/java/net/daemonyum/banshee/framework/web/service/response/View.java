package net.daemonyum.banshee.framework.web.service.response;

import lombok.Getter;

@Getter
public class View {

	public static final View BLANK = View.dispatch("");

	public static final View MAP = View.mapping();

	private String view;	

	private View(String view) {
		this.view = view;
	}

	public static View dispatch(String newView) {
		return new View(newView);
	}

	private static View mapping() {
		return new View(null);
	}

}
