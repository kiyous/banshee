package net.daemonyum.banshee.framework.web.utils;

/**
 * 
 * @author Kiyous
 *
 */
public class Constant {
	
	public static final String BASENAME = "error-message";	
	
	public static final String BANSHEE_SESSION_CONFIRM_MESSAGE_PARAMETER = "BANSHEE_SESSION_CONFIRM_MESSAGE";
	public static final String BANSHEE_SESSION_ERRORS_MESSAGE_PARAMETER = "BANSHEE_SESSION_ERRORS_MESSAGE";	
	public static final String SECURITY_EXCEPTION_MESSAGE = Config.SECURITY_ERROR_MESSAGE;
	
}
