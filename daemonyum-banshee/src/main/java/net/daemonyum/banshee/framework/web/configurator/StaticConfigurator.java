package net.daemonyum.banshee.framework.web.configurator;

public interface StaticConfigurator extends Enum {
	
	String getPath();

}
