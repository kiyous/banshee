package net.daemonyum.banshee.framework.web.bean;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;

import net.daemonyum.manticora.framework.web.bean.AbstractManticoraBean;

/**
 * Represente les données d'un formulaire à traiter.
 * @author Kiyous
 *
 */
public abstract class FormBean extends AbstractManticoraBean {

	private static class FailFormBean extends FormBean {
		@Override
		public boolean isValid() {
			return false;
		}
	}

	private static class ValidFormBean extends FormBean {
		@Override
		public boolean isValid() {
			return true;
		}
	}

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(FormBean.class);

	/**
	 * Affiche le contenu des attributs sous la forme d'une chaine de caractère.
	 */
	@Override
	public String toString() {

		String toString = "";
		toString+= this.getClass().getName()+"\n";
		for(Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				toString += field.getName()+" : "+ field.get(this) +"\n";
			} catch (Exception e) {
				LOGGER.warn("", e);
			}

		}

		return toString;
	}

	public static FormBean failBean() {
		return new FormBean.FailFormBean();
	}

	public static FormBean validBean() {
		return new FormBean.ValidFormBean();
	}
}
