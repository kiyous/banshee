package net.daemonyum.banshee.framework.web.configurator;

import net.daemonyum.manticora.framework.web.service.servlet.ManticoraServlet;

/**
 * Configuration d'une servlet de l'application.
 * @author Kiyous
 *
 */
public interface ServletConfigurator extends Enum  {
	
	Class<? extends ManticoraServlet> getServlet();	
	String getUrlWithoutExtension();	
	String getApplicationLink();	
	
}
