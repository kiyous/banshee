package net.daemonyum.banshee.framework.web.configurator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;


/**
 * Contient la liste des paramètres de configuration nécessaire au démarrage d'une application Banshee
 * @author Kiyous
 *
 */
@AllArgsConstructor
@Builder
@Getter
public final class Configurator {
	
	private ServletConfigurator [] servletConf;
	JSPConfigurator [] jspConf;
	RedirectConfigurator [] redirectConf;
	JSPConfigurator [] mobileConf;	
	StaticConfigurator [] staticConf;

}
