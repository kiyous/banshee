package net.daemonyum.banshee.framework.web.configurator;

import net.daemonyum.banshee.framework.web.bean.FormBean;

/**
 * Configuration d'une redirection de page apr�s l'execution d'une action au sein de l'application.
 * @author Kiyous
 *
 */
public interface RedirectConfigurator extends Enum {	

	/**
	 * Change de manière perpétuelle la redirection d'erreur.
	 * @param servletConfigurator
	 */
	void changeErrorServlet(ServletConfigurator servletConfigurator) ;
	/**
	 * Change de manière perpétuelle la redirection d'execution.
	 * @param servletConfigurator
	 */
	void changeSuccessServlet(ServletConfigurator servletConfigurator) ;
	ServletConfigurator getErrorServlet() ;

	Class<? extends FormBean> getForm();
	ServletConfigurator getSuccessServlet() ;

}
