package net.daemonyum.banshee.framework.web.configurator.override;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CombinedView {

	private PackagedURL packagedURL;
	private String specificURL;
}