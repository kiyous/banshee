package net.daemonyum.banshee.framework.web.configurator.override;

import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import net.daemonyum.banshee.framework.web.configurator.ServletConfigurator;

@Builder
@Getter
@Setter
public class PackagedURL {

	private Map<String,String> parameters;
	private ServletConfigurator servletConfig;
}