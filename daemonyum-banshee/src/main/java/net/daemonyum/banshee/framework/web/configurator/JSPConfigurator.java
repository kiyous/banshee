package net.daemonyum.banshee.framework.web.configurator;

import net.daemonyum.manticora.framework.web.service.view.ServletFile;

/**
 * Configuration d'une vue de l'application
 * @author Kiyous
 *
 */
public interface JSPConfigurator extends ServletFile, Enum {	
	
	boolean hasMobileVersion();
	
}
